# Fichier de configuration compatible avec les version de openSSH antérieurs à 7.3
# .ssh/config générique mis à votre disposition
# par les gentils membres actifs du Cr@ns
# Vous pouvez l'utilisez, le redistribuer, le modifier à votre convenance.
# Des questions, des suggestions : {nounou,ca}@lists.crans.org
# Typiquement, vous pouvez proposer d'ajouter la conf pour accéder à votre département
# Licence : WTFPL

# Les sections commentées par #~# sont des features qui ne sont pas activées
# par défaut. Sentez-vous libre de les décommenter pour les utiliser.

# À noter que la plupart des serveurs présents dedans sont des serveurs
# du Cr@ns, qui risquent donc d'intéresser essentiellement des membres actifs.
# Pensez à remplacer "loginCr@ns", "loginFedeRez", "loginBDE" et "loginENS" par
# les logins idoines.

#------------------------------------------------------------------------------

# Les options qui ne sont pas indentées sont activées pour toutes les connexions
# Mêmes celles qui ne correspondent à aucun bloc plus bas
# Activer la compression des données transmises lorsque c'est possible
Compression yes

#~# # Afficher la fingerprint du serveur sous la forme d'un ASCII art
#~# VisualHostKey yes

# Ne pas hasher les noms des machines auxquelles on se connecte dans
# le fichier known_hosts
HashKnownHosts no

# Vérifier la concordance du champ DNS SSHFP de la machine (si existant)
# et du fingerprint présenté par le serveur
VerifyHostKeyDNS yes

#~# # Utilise le system de connection Master pour accélérer les connexions multiple
#~# # a un même host.  Attention toutes les connexions sont liés à la première, la
#~# # fermer, fermera toutes les connexions. (voir option suivante)
#~# ControlMaster yes

#~# # Maintient la connexion maitre après la fermeture de la connexion initiale.
#~# # no: la connexion est fermé
#~# # yes: la connexion reste ouverte en arrière plan jusqu'à fermeture explicite
#~# # n: la connexion reste ouverte en arrière plan n secondes si non utilisé
#~# ControlPersist 60

#~# # Certaines QuelqueChoseBox tuent les connexion TCP inactives depuis
#~# # trop longtemps.
#~# # Cette option fait en sorte d'envoyer toutes les 60 secondes un paquet
#~# # sur la connexion, pour la garder vivante.
#~# ServerAliveInterval 60

#~# # Abandonner au bout de 3 échecs (= considérer la connexion comme morte)
#~# ServerAliveCountMax 3

# Les options suivantes apparaissent dans les blocs
# Host = commence un bloc avec les noms qui utiliseront ce bloc
# HostName = nom réellement utilisé pour se connecter au serveur (ou son IP)
# User = nom d'utilisateur distant
# Port = port de connexion (pour override le port 22)
# ForwardAgent = forwarder l'agent ssh sur la machine
#            (il vaut mieux qu'elle et ses administrateurs soient de confiance)
# ProxyCommand = pour passer par un autre serveur intermédiaire
#                (pour un serveur qui ne peut pas être contacté directement)

#~# # +-----------------+
#~# # | Serveurs du BDE |
#~# # +-----------------+
#~# # Serveurs du BDE accessibles aux respos info
#~# 
#~# Host kfet kfet.crans.org kfet1
#~#     HostName kfet.crans.org
#~#     User bde
#~# 
#~# Host kfet-bureau kfet-bureau.crans.org
#~#     HostName kfet.crans.org
#~#     User bde
#~# 
#~# Host bde2 bde2-virt note note-virt kfet-ics doc-bde ldap-bde bde2.crans.org bde2-virt.crans.org note.crans.org
#~#     HostName bde2-virt.crans.org
#~#     User loginBDE
#~#     ForwardAgent yes
#~# 
#~# Host bde3-virt digicode bde3-virt.crans.org
#~#     HostName bde3-virt.crans.org
#~#     User loginBDE
#~#     ForwardAgent yes
#~# 
#~# Host bde3 bde3.crans.org
#~#     HostName bde3.crans.org
#~#     User loginBDE
#~#     ForwardAgent yes
#~# 
#~# Host bde-test bde-test-virt note-test note-dev bde-test.crans.org bde-test-virt.crans.org note-test.crans.org
#~#     HostName bde-test-virt.crans.org
#~#     User loginBDE
#~#     ForwardAgent yes
#~# 
#~# Host sauron sauron.crans.org
#~#     HostName sauron.crans.org
#~#     User loginBDE
#~#     ForwardAgent yes

# +-------------------+
# | Serveurs du Cr@ns |
# +-------------------+
# Accessible aux apprentis
# sauf zamok et ssh2, accessibles à tous les adhérents

Host alice alice.crans.org
    HostName alice.crans.org
    User loginCr@ns
    ForwardAgent yes

Host alice.adm alice.adm.crans.org
    HostName alice.adm.crans.org
    User loginCr@ns
    ForwardAgent yes

Host apprentis apprentis.crans.org
    HostName apprentis.crans.org
    User loginCr@ns

Host backbone backbone.crans.org
    HostName backbone.crans.org
    User loginCr@ns
    ForwardAgent yes

Host backbone.wifi backbone.wifi.crans.org
    HostName backbone.wifi.crans.org
    User loginCr@ns
    ForwardAgent yes

Host bakdaur bakdaur.crans.org
    HostName bakdaur.crans.org
    User loginCr@ns
    ForwardAgent yes

Host bakdaur.adm bakdaur.adm.crans.org
    HostName bakdaur.adm.crans.org
    User loginCr@ns
    ForwardAgent yes

Host cas cas-srv auth login cas.crans.org login.crans.org auth.crans.org cas-srv.crans.org
    HostName cas-srv.crans.org
    User loginCr@ns
    ForwardAgent yes

Host cas.adm cas.adm.crans.org
    HostName cas.adm.crans.org
    User loginCr@ns
    ForwardAgent yes

Host charybde ftp ftp.crans.ens-cachan.fr karibe mirror ntp xn--hxamgu1bpkn charybde.crans.org karibe.crans.org xn--hxamgu1bpkn.crans.org ftp.crans.org mirror.crans.org ftp.crans.ens-cachan.fr ntp.crans.org
    HostName charybde.crans.org
    User loginCr@ns
    ForwardAgent yes

Host charybde.adm ftp.adm mirror.adm ntp.adm charybde.adm.crans.org ftp.adm.crans.org mirror.adm.crans.org ntp.adm.crans.org
    HostName charybde.adm.crans.org
    User loginCr@ns
    ForwardAgent yes

Host civet rabbitmq vvvvvv civet.crans.org vvvvvv.crans.org rabbitmq.crans.org
    HostName civet.crans.org
    User loginCr@ns
    ForwardAgent yes

Host civet.adm rabbitmq.adm civet.adm.crans.org rabbitmq.adm.crans.org
    HostName civet.adm.crans.org
    User loginCr@ns
    ForwardAgent yes

Host dhcp dhcp.crans.org
    HostName dhcp.crans.org
    User loginCr@ns
    ForwardAgent yes

Host dhcp.adm dhcp.adm.crans.org
    HostName dhcp.adm.crans.org
    User loginCr@ns
    ForwardAgent yes

Host digicode-4j digicode-4j.crans.org
    HostName digicode-4j.crans.org
    User loginCr@ns
    ForwardAgent yes

Host digicode-4j.adm digicode-4j.adm.crans.org
    HostName digicode-4j.adm.crans.org
    User loginCr@ns
    ForwardAgent yes

Host ethercalc ethercalc-srv ethercalc.crans.org ethercalc-srv.crans.org
    HostName ethercalc-srv.crans.org
    User loginCr@ns
    ForwardAgent yes

Host ethercalc.adm ethercalc.adm.crans.org
    HostName ethercalc.adm.crans.org
    User loginCr@ns
    ForwardAgent yes

Host freebox freebox.crans.org
    HostName freebox.crans.org
    User loginCr@ns
    ForwardAgent yes

Host frontdaur frontdaur.crans.org
    HostName frontdaur.crans.org
    User loginCr@ns
    ForwardAgent yes

Host frontdaur.adm frontdaur.adm.crans.org
    HostName frontdaur.adm.crans.org
    User loginCr@ns
    ForwardAgent yes

Host ft ft.crans.org
    HostName ft.crans.org
    User loginCr@ns
    ForwardAgent yes

Host ft.adm ft.adm.crans.org
    HostName ft.adm.crans.org
    User loginCr@ns
    ForwardAgent yes

Host fy munin icinga2 weathermap fy.crans.org munin.crans.org icinga2.crans.org weathermap.crans.org
    HostName fy.crans.org
    User loginCr@ns
    ForwardAgent yes

Host fy.adm fy.adm.crans.org
    HostName fy.adm.crans.org
    User loginCr@ns
    ForwardAgent yes

Host fz fz.crans.org
    HostName fz.crans.org
    User loginCr@ns
    ForwardAgent yes

Host fz.adm fz.adm.crans.org
    HostName fz.adm.crans.org
    User loginCr@ns
    ForwardAgent yes

Host gulp gulp.crans.org
    HostName gulp.crans.org
    User loginCr@ns
    ForwardAgent yes

Host gulp.adm gulp.adm.crans.org
    HostName gulp.adm.crans.org
    User loginCr@ns
    ForwardAgent yes

Host jitsi jitsi.crans.org
    HostName jitsi.crans.org
    User loginCr@ns
    ForwardAgent yes

Host jitsi.adm jitsi.adm.crans.org
    HostName jitsi.adm.crans.org
    User loginCr@ns
    ForwardAgent yes

Host gitzly git gitlab gitzly.crans.org git.crans.org
    HostName gitzly.crans.org
    User loginCr@ns
    ForwardAgent yes

Host gitzly.adm git.adm gitlab.adm gitzly.adm.crans.org git.adm.crans.org gitlab.adm.crans.org
    HostName gitzly.adm.crans.org
    User loginCr@ns
    ForwardAgent yes

Host ipv6 ipv6-zayo ipv6.crans.org ipv6-zayo.crans.org
    HostName ipv6-zayo.crans.org
    User loginCr@ns
    ForwardAgent yes

Host ipv6.adm ipv6-zayo.adm ipv6.adm.crans.org ipv6-zayo.adm.crans.org
    HostName ipv6-zayo.adm.crans.org
    User loginCr@ns
    ForwardAgent yes

Host irc qwebirc web-irc irc.crans.org qwebirc.crans.org web-irc.crans.org
    HostName irc.crans.org
    User loginCr@ns
    ForwardAgent yes

Host irc.adm irc.adm.crans.org
    HostName irc.adm.crans.org
    User loginCr@ns
    ForwardAgent yes

Host kenobi pad pastebin zero kenobi.crans.org pad.crans.org zero.crans.org pastebin.crans.org
    HostName kenobi.crans.org
    User loginCr@ns
    ForwardAgent yes

Host kenobi.adm kenobi.adm.crans.org
    HostName kenobi.adm.crans.org
    User loginCr@ns
    ForwardAgent yes

Host kiwi kiwi.crans.org wiki wikipedia wiki.crans.org wikipedia.crans.org
    HostName kiwi.crans.org
    User loginCr@ns
    ForwardAgent yes

Host kiwi.adm wiki.adm kiwi.adm.crans.org wiki.adm.crans.org
    HostName kiwi.adm.crans.org
    User loginCr@ns
    ForwardAgent yes

Host mediadrop mediadrop-srv video mediadrop.crans.org video.crans.org mediadrop-srv.crans.org video-srv.crans.org
    HostName mediadrop-srv.crans.org
    User loginCr@ns
    ForwardAgent yes

Host mediadrop.adm mediadrop.adm.crans.org
    HostName mediadrop.adm.crans.org
    User loginCr@ns
    ForwardAgent yes

Host nem nem.crans.org
    HostName nem.crans.org
    User loginCr@ns
    ForwardAgent yes

Host nem.adm nem.adm.crans.org
    HostName nem.adm.crans.org
    User loginCr@ns
    ForwardAgent yes

Host news web-news webnews news.crans.org web-news.crans.org webnews.crans.org
    HostName news.crans.org
    User loginCr@ns
    ForwardAgent yes

Host news.adm web-news.adm webnews.adm news.adm.crans.org webnews.adm.crans.org web-news.adm.crans.org
    HostName news.adm.crans.org
    User loginCr@ns
    ForwardAgent yes

Host niomniom autoconfig autostatus doc niomniom.crans.org autostatus.crans.org doc.crans.org autoconfig.crans.org
    HostName niomniom.crans.org
    User loginCr@ns
    ForwardAgent yes

Host niomniom.adm niomniom.adm.crans.org
    HostName niomniom.adm.crans.org
    User loginCr@ns
    ForwardAgent yes

Host odlyd odlyd.crans.org
    HostName odlyd.crans.org
    User loginCr@ns
    ForwardAgent yes

Host odlyd.adm upload.adm odlyd.adm.crans.org upload.adm.crans.org
    HostName odlyd.adm.crans.org
    User loginCr@ns
    ForwardAgent yes

Host odlyd.wifi odlyd.wifi.crans.org
    HostName odlyd.wifi.crans.org
    User loginCr@ns
    ForwardAgent yes

Host owl imap pop owl.crans.org pop.crans.org imap.crans.org
    HostName owl.crans.org
    User loginCr@ns
    ForwardAgent yes

Host owl.adm imap.adm pop.adm owl.adm.crans.org imap.adm.crans.org pop.adm.crans.org
    HostName owl.adm.crans.org
    User loginCr@ns
    ForwardAgent yes

Host owncloud owcloud-srv owncloud.crans.org owncloud-srv.crans.org
    HostName owncloud-srv.crans.org
    User loginCr@ns
    ForwardAgent yes

Host owncloud.adm owncloud.adm.crans.org
    HostName owncloud.adm.crans.org
    User loginCr@ns
    ForwardAgent yes

Host proxy proxy.crans.org
    HostName proxy.crans.org
    User loginCr@ns
    ForwardAgent yes

Host redisdead list liste listes lists smtp redisdead.crans.org lists.crans.org smtp.crans.org liste.crans.org listes.crans.org list.crans.org
    HostName redisdead.crans.org
    User loginCr@ns
    ForwardAgent yes

Host redisdead.adm list.adm liste.adm listes.adm lists.adm smtp.adm redisdead.adm.crans.org lists.adm.crans.org smtp.adm.crans.org listes.adm.crans.org list.adm.crans.org liste.adm.crans.org
    HostName redisdead.adm.crans.org
    User loginCr@ns
    ForwardAgent yes

Host roundcube roundcube-srv roundcube.crans.org roundcube-srv.crans.org
    HostName roundcube-srv.crans.org
    User loginCr@ns
    ForwardAgent yes

Host roundcube.adm roundcube.adm.crans.org
    HostName roundcube.adm.crans.org
    User loginCr@ns
    ForwardAgent yes

Host routeur deconnexion routeur.crans.org deconnexion.crans.org
    HostName routeur.crans.org
    User loginCr@ns
    ForwardAgent yes

Host routeur.adm routeur.adm.crans.org
    HostName routeur.adm.crans.org
    User loginCr@ns
    ForwardAgent yes

Host silice silice.crans.org
    HostName silice.crans.org
    User loginCr@ns
    ForwardAgent yes

Host silice.adm silice.adm.crans.org
    HostName silice.adm.crans.org
    User loginCr@ns
    ForwardAgent yes

Host soyouz soyoustart soyouz.crans.org soyoustart.crans.org
    HostName soyouz.crans.org
    User loginCr@ns
    ForwardAgent yes

Host soyouz.adm soyoustart.adm soyouz.adm.crans.org soyoustart.adm.crans.org
    HostName soyouz.adm.crans.org
    User loginCr@ns
    ForwardAgent yes

Host ssh2 mosh ssh2.crans.org mosh.crans.org
    # Un serveur ssh qui n'est qu'un nat vers 138.231.136.1:22
    # pour passer à travers les blocages de ports (443 = https)
    HostName 138.231.136.2
    Port 443
    User loginCr@ns
    ForwardAgent yes

Host stitch stitch.crans.org
    HostName stitch.crans.org
    User loginCr@ns
    ForwardAgent yes

Host stitch.adm stitch.adm.crans.org
    HostName stitch.adm.crans.org
    User loginCr@ns
    ForwardAgent yes

Host thot logs psuornotpsu thot.crans.org psuornotpsu.crans.org logs.crans.org
    HostName thot.crans.org
    User loginCr@ns
    ForwardAgent yes

Host thot.adm logs.adm pgsql.adm psuornotpsu.adm thot.adm.crans.org psuornotpsu.adm.crans.org pgsql.adm.crans.org logs.adm.crans.org
    HostName thot.adm.crans.org
    User loginCr@ns
    ForwardAgent yes

Host titanic titanic.crans.org
    HostName titanic.crans.org
    User loginCr@ns
    ForwardAgent yes

Host titanic.adm titanic.adm.crans.org
    HostName titanic.adm.crans.org
    User loginCr@ns
    ForwardAgent yes

Host unifi unifi.crans.org
    HostName unifi.crans.org
    User loginCr@ns
    ForwardAgent yes

Host unifi.adm unifi.adm.crans.org
    HostName unifi.adm.crans.org
    User loginCr@ns
    ForwardAgent yes

Host vo calendrier intranet-dev news-dev openid-dev weavesync vo.crans.org weavesync.crans.org intranet-dev.crans.org calendrier.crans.org openid-dev.crans.org news-dev.crans.org
    HostName vo.crans.org
    User loginCr@ns
    ForwardAgent yes

Host vo.adm vo.adm.crans.org
    HostName vo.adm.crans.org
    User loginCr@ns
    ForwardAgent yes

Host vulcain vulcain.crans.org
    HostName vulcain.crans.org
    User loginCr@ns
    ForwardAgent yes

Host vulcain.adm vulcain.adm.crans.org
    HostName vulcain.adm.crans.org
    User loginCr@ns
    ForwardAgent yes

Host xmpp aim conference conference.jabber icq jabber xmpp.crans.org icq.crans.org aim.crans.org conference.crans.org jabber.crans.org yahoo.crans.org chat.yahoo.crans.org conference.jabber.crans.org
    HostName xmpp.crans.org
    User loginCr@ns
    ForwardAgent yes

Host xmpp.adm jabber.adm xmpp.adm.crans.org jabber.adm.crans.org
    HostName xmpp.adm.crans.org
    User loginCr@ns
    ForwardAgent yes

Host zamok ajaxterm bda-paris blogs clubs clubs.ens-cachan.fr mysql perso pot-vieux ssh zamok.crans.org ssh.crans.org perso.crans.org clubs.ens-cachan.fr www.clubs.ens-cachan.fr mysql.crans.org blogs.crans.org bda-paris.crans.org ajaxterm.crans.org clubs.crans.org pot-vieux.crans.org
    HostName zamok.crans.org
    User loginCr@ns
    ForwardAgent yes

Host zamok.adm users.adm zamok.adm.crans.org users.adm.crans.org
    HostName zamok.adm.crans.org
    User loginCr@ns
    ForwardAgent yes


# +-------------------------+
# | Serveurs Cr@ns adm-only |
# +-------------------------+
# Accessibles aux apprentis, mais ne sont que sur le VLAN adm
# d'où la ProxyCommand

Host cochon.adm cochon.adm.crans.org
    HostName cochon.adm.crans.org
    User loginCr@ns
    ForwardAgent yes
    ProxyCommand ssh odlyd.crans.org -W %h:%p

Host cups.adm cups.adm.crans.org
    HostName cups.adm.crans.org
    User loginCr@ns
    ForwardAgent yes
    ProxyCommand ssh odlyd.crans.org -W %h:%p

Host eap.adm eap eap eap.adm.crans.org
    HostName eap.adm.crans.org
    User loginCr@ns
    ForwardAgent yes
    ProxyCommand ssh odlyd.crans.org -W %h:%p

Host horde horde.adm webmail horde.crans.org horde.adm.crans.org webmail.crans.org
    HostName horde.adm.crans.org
    User loginCr@ns
    ForwardAgent yes
    ProxyCommand ssh odlyd.crans.org -W %h:%p

Host *-ilo
    HostName %h.adm.crans.org
    User Administrator
    PasswordAuthentication yes
    ChallengeResponseAuthentication no
    GSSAPIAuthentication no
    HostbasedAuthentication no
    PubkeyAuthentication no
    Compression no
    ForwardAgent no
    ForwardX11 no
    KexAlgorithms diffie-hellman-group1-sha1
    MACs hmac-md5,hmac-sha1
    Ciphers aes128-cbc,3des-cbc
    HostKeyAlgorithms ssh-rsa,ssh-dss
    ProxyCommand ssh odlyd.crans.org -W %h:%p

Host omnomnom.adm omnomnom.adm.crans.org
    HostName omnomnom.adm.crans.org
    User loginCr@ns
    ForwardAgent yes
    ProxyCommand ssh odlyd.crans.org -W %h:%p

Host phabricator todo tracker.adm tracker.adm.crans.org
    HostName tracker.adm.crans.org
    User loginCr@ns
    ForwardAgent yes
    ProxyCommand ssh odlyd.crans.org -W %h:%p

Host radius.adm radius radius radius.adm.crans.org
    HostName radius.adm.crans.org
    User loginCr@ns
    ForwardAgent yes
    ProxyCommand ssh odlyd.crans.org -W %h:%p

Host re2o re2o.adm re2o.adm.crans.org intranet intranet.adm intranet.adm.crans.org
    HostName re2o.adm.crans.org
    User loginCr@ns
    ForwardAgent yes
    ProxyCommand ssh odlyd.crans.org -W %h:%p

Host re2o-bcfg2 re2o-bcfg2.adm re2o-bcfg2.adm.crans.org bcfg2.adm bcfg2 bcfg2 bcfg2.adm.crans.org
    HostName re2o-bcfg2.adm.crans.org
    User loginCr@ns
    ForwardAgent yes
    ProxyCommand ssh odlyd.crans.org -W %h:%p
    HostKeyAlgorithms ssh-rsa

Host slon.adm slon slon slon.adm.crans.org
    HostName slon.adm.crans.org
    User loginCr@ns
    ForwardAgent yes
    ProxyCommand ssh odlyd.crans.org -W %h:%p

Host vert.adm ldap.adm vert zelda.adm vert vert.adm.crans.org ldap.adm.crans.org zelda.adm.crans.org
    HostName vert.adm.crans.org
    User loginCr@ns
    ForwardAgent yes
    ProxyCommand ssh odlyd.crans.org -W %h:%p

Host ytrap-llatsni.adm ytrap-llatsni.adm.crans.org
    HostName ytrap-llatsni.adm.crans.org
    User loginCr@ns
    ForwardAgent yes
    ProxyCommand ssh odlyd.crans.org -W %h:%p

Host zbee-idrac.adm zbee-idrac zbee-idrac zbee-idrac.adm.crans.org
    HostName zbee-idrac.adm.crans.org
    User loginCr@ns
    ForwardAgent yes
    ProxyCommand ssh odlyd.crans.org -W %h:%p

Host zbee.adm fx.adm nfs.adm zbee zbee zbee.adm.crans.org nfs.adm.crans.org fx.adm.crans.org
    HostName zbee.adm.crans.org
    User loginCr@ns
    ForwardAgent yes
    ProxyCommand ssh odlyd.crans.org -W %h:%p


#~# # +---------+
#~# # | FedeRez |
#~# # +---------+
#~# # Accessibles aux membres de l'équipe d'admin de FedeRez
#~# 
#~# Host hexagon hexagon.federez.net
#~#     HostName hexagon.federez.net
#~#     User loginFedeRez
#~#     ForwardAgent yes
#~# 
#~# Host quigon quigon.federez.net
#~#     HostName quigon.federez.net
#~#     User loginFedeRez
#~#     ForwardAgent yes
#~# 
#~# Host nonagon nonagon.federez.net
#~#     HostName nonagon.federez.net
#~#     User loginFedeRez
#~#     ForwardAgent yes
#~# 
#~# Host octogon octogon.federez.net
#~#     HostName octogon.federez.net
#~#     User loginFedeRez
#~#     ForwardAgent yes
#~# 
#~# Host pentagon pentagon.federez.net
#~#     HostName pentagon.federez.net
#~#     User loginFedeRez
#~#     ForwardAgent yes
#~# 
#~# Host parangon parangon.federez.net
#~#     HostName parangon.federez.net
#~#     User loginFedeRez
#~#     ForwardAgent yes
#~# 
#~# Host federez-test federez-test.federez.net
#~#     HostName federez-test.federez.net
#~#     User loginFedeRez
#~#     ForwardAgent yes
#~# 


# +-------------------+
# | Serveurs de l'ENS |
# +-------------------+
# Accessibles aux élèves de l'ENS

Host tselin tselin.clietu tselin.clietu.ens-cachan.fr acces1.rip.ens-cachan.fr acces1 rip
    HostName tselin.clietu.ens-cachan.fr
    User loginENS
    ProxyCommand ssh loginENS@tahines.ens-cachan.fr -W %h:%p

Host tahines.ens-cachan.fr tahines2 tahines
    HostName tahines.ens-cachan.fr
    User loginENS


#~# # +------------------+
#~# # | Département info |
#~# # +------------------+
#~# # Machines du département informatique de l'ENSC
#~# # Accesibles aux A0
#~# 
#~# Host info21 infossh ssh.dptinfo ssh.dptinfo.ens-cachan.fr
#~#     #HostName 138.231.36.60
#~#     HostName ssh.dptinfo.ens-cachan.fr
#~#     User loginENS
#~#     ForwardAgent yes
#~# 
#~# # Le ! permet de ne pas capturer ssh.dptinfo dans *.dptinfo
#~# # Sinon on crée une boucle
#~# Host *.dptinfo !ssh.dptinfo
#~#     HostName %h.ens-cachan.fr
#~#     User loginENS
#~#     ProxyCommand ssh loginENS@ssh.dptinfo.ens-cachan.fr -W %h:%p
#~#     ForwardAgent yes
#~# 
#~# Host *.dptinfo.ens-cachan.fr !ssh.dptinfo.ens-cachan.fr
#~#     HostName %h
#~#     User loginENS
#~#     ProxyCommand ssh loginENS@ssh.dptinfo.ens-cachan.fr -W %h:%p
#~#     ForwardAgent yes


# END (utile pour éviter les merge conflicts)

