# .profile générique mis à votre disposition
# par les gentils membres actifs du Cr@ns
# Vous pouvez l'utiliser, le redistribuer, le modifier à votre convenance.
# Des questions, des suggestions : {nounou,ca}@lists.crans.org
# Licence : WTFPL

# Les sections commentées par #~# sont des features qui ne sont pas activées
# par défaut. Sentez-vous libre de les décommenter pour les utiliser.

#------------------------------------------------------------------------------

# Si le shell est bash, lire le fichier de configuration (s'il existe)
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
	. "$HOME/.bashrc"
    fi
fi

# Si on a un dossier ~/bin, l'ajouter à la liste des dossiers
# dans lesquels le shell cherche les commandes
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi
